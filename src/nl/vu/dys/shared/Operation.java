package nl.vu.dys.shared;

/* Enumerator defines the actions to be taken by a client, whether it is a
*  client side communication or a server side communication*/

public enum Operation {
    KILLED(0), UP(1),
    DOWN(2), RIGHT(3),
    LEFT(4), HEAL(5),
    STRIKE(6),SYNC(7),
    UPDATE(8), CONNECT(9),
    DISCONNECT(10), ACK(11),
    SHUTDOWN(12), COORDS_OCCUPIED(13);
    byte operationNr;
    Operation(int nr) { this.operationNr = (byte)nr; }
    public byte getOperationNr() { return operationNr; }
    public static Operation getOperation(byte nr) {
        switch (nr) {
            case 0: return KILLED;
            case 1: return UP;
            case 2: return DOWN;
            case 3: return RIGHT;
            case 4: return LEFT;
            case 5: return HEAL;
            case 6: return STRIKE;
            case 7: return SYNC;
            case 8: return UPDATE;
            case 9: return CONNECT;
            case 10: return DISCONNECT;
            case 11: return ACK;
            case 12: return SHUTDOWN;
            case 13: return COORDS_OCCUPIED;
            default: throw new UnsupportedOperationException("Unsupported game operation.");
        }
    }
}
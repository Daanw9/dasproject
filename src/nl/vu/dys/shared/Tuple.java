package nl.vu.dys.shared;

public class Tuple<K,B> {
    public K a;
    public B b;
    Tuple(K a, B b) {
        this.a = a;
        this.b = b;
    }
    public static<K,B> Tuple<K,B> tuple(K valOne, B valTwo) {
        return new Tuple<>(valOne, valTwo);
    }

    @Override
    public boolean equals(Object obj) {
        return (obj instanceof Tuple) && a.equals(((Tuple)obj).a);
    }

    @Override
    public int hashCode() {
        return a.hashCode();
    }
}

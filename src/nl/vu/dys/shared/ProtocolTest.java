package nl.vu.dys.shared;

import org.junit.Assert;
import org.junit.Test;

import java.nio.ByteBuffer;
import java.util.*;

import static org.junit.Assert.*;

public class ProtocolTest {
    public static final int DESTINATION_PORT = 6777;
    public static final int SOURCEPORT = 6667;

    @Test
    public void decode() throws Exception {
        Protocol p = Protocol.getNewInstance();
        p.clientId = 32932;
        p.targetClientId=42;
        p.ackNr = 291;
        p.sequenceNr = 33;
        p.destinationPort=2600;
        p.sourcePort = 5100;
        p.operation = Operation.DOWN;
        p.payload = PlayerData.encode(new PlayerData(38238,(byte)23,(byte)24,(byte)25,(byte)26, (byte)0));
        Protocol d = Protocol.decode(Protocol.encode(p));
        assertEquals(d.clientId, p.clientId);
        assertEquals(d.targetClientId, p.targetClientId);
        assertEquals(d.ackNr, p.ackNr);
        assertEquals(d.sequenceNr, p.sequenceNr);
        assertEquals(d.destinationPort, p.destinationPort);
        assertEquals(d.sourcePort, p.sourcePort);
        assertEquals(d.operation, p.operation);
        PlayerData pdd = PlayerData.decode(d.payload);
        assertEquals( true, pdd.playerId==38238&&pdd.x==23&&pdd.y==24&&pdd.hp==25&&pdd.ap==26&&pdd.isDragon==0);
    }

    @Test
    public void encode() throws Exception {
        Protocol p = Protocol.getNewInstance();
        p.clientId = 32932;
        p.targetClientId=32;
        p.ackNr = 291;
        p.sequenceNr = 33;
        p.destinationPort=2600;
        p.sourcePort = 5100;
        p.operation = Operation.UP;
        p.payload = PlayerData.encode(new PlayerData(p.clientId,(byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25)));
        byte[] encoded = Protocol.encode(p);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 0, 4).getInt(), p.sourcePort);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 4, 4).getInt(), p.destinationPort);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 8, 4).getInt(), p.sequenceNr);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 12, 4).getInt(), p.ackNr);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 16, 4).getInt(), p.clientId);
        Assert.assertEquals(ByteBuffer.wrap(encoded, 20, 4).getInt(), p.targetClientId);
        Assert.assertEquals(Operation.getOperation(ByteBuffer.wrap(encoded).get(24)), p.operation);
        short payloadSize=ByteBuffer.wrap(encoded, 25, 2).getShort();
        Assert.assertEquals(PlayerData.MTU,payloadSize);
        Assert.assertEquals(0,ByteBuffer.wrap(Arrays.copyOfRange(encoded,27,27+payloadSize)).compareTo(ByteBuffer.wrap(p.payload)));
    }

    public static Protocol generateProtocol(int sourcePort, int destinationPort) {
        Protocol pr = Protocol.getNewInstance();
        pr.clientId = Math.abs(UUID.randomUUID().hashCode());
        pr.targetClientId = 0;
        pr.ackNr = new Random().nextInt(5000);
        pr.sequenceNr = Math.abs(UUID.randomUUID().hashCode());
        pr.destinationPort=destinationPort;
        pr.sourcePort = sourcePort;
        pr.operation = Operation.CONNECT;
        pr.payload = PlayerData.encode(new PlayerData(pr.clientId,(byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25),
                (byte)new Random().nextInt(25)));
        return pr;
    }

    public static Protocol generateProtocol() {
        return generateProtocol(SOURCEPORT,DESTINATION_PORT);
    }
}
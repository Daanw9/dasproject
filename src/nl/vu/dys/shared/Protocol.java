package nl.vu.dys.shared;

import java.nio.ByteBuffer;
import java.util.Arrays;

/*
* Protocol Serializes and De-serializes a Datagram Packet containing a payload.
* At any given time, the 'protocol' packet may be in an encoded or decoded state
*
*/
public class Protocol implements Comparable<Protocol> {
    public static int MTU = 5000; /* bytes, MTU :  Maximum Transmission Unit*/
    public int sourcePort, destinationPort, sequenceNr, ackNr, clientId, targetClientId;
    public byte[] payload = new byte[0];
    public Operation operation;     // Enumerate Legal operations


    public static Protocol getNewInstance() {return new Protocol(); }

    // De-Serialize a datagram packet into its components and return them
    public static Protocol decode(byte[] bytes) {
        Protocol p = getNewInstance();
        ByteBuffer bb = ByteBuffer.wrap(bytes,0,MTU);
        p.sourcePort = bb.getInt();
        p.destinationPort = bb.getInt();
        p.sequenceNr = bb.getInt();
        p.ackNr = bb.getInt();
        p.clientId = bb.getInt();
        p.targetClientId = bb.getInt();
        p.operation = Operation.getOperation(bb.get());
        p.payload = Arrays.copyOfRange(bytes,27,27+bb.getShort());
        return p;
    }

    // Serialize a datagram's Components and return the byte array
    public static byte[] encode(Protocol p) {
        ByteBuffer bb = ByteBuffer.allocate(MTU);
        bb.putInt(p.sourcePort);
        bb.putInt(p.destinationPort);
        bb.putInt(p.sequenceNr);
        bb.putInt(p.ackNr);
        bb.putInt(p.clientId);
        bb.putInt(p.targetClientId);
        bb.put(p.operation.getOperationNr());
        bb.putShort((short)p.payload.length);
        bb.put(p.payload);
        return bb.array();
    }

    // Hashcodes for comparission are defined to be client ID's
    @Override
    public int hashCode() {
        return clientId;
    }


    // Compare hashcode for current 'Protocol' Datagram with another 'Protocol' Datagram
    @Override
    public boolean equals(Object obj) {
        return Integer.compare(hashCode(),obj.hashCode())==0;
    }

    // Compare Datagram Sequence Numbers
    @Override
    public int compareTo(Protocol o) {
        return Integer.compare(sequenceNr, o.sequenceNr);
    }


    @Override
    public String toString() {
        return String.format("Clientid:%s,targetId:%s,operation:%s",clientId,targetClientId,operation.toString());
    }
}

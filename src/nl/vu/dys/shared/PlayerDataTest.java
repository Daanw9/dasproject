package nl.vu.dys.shared;

import org.junit.Assert;
import org.junit.Test;

import java.util.Random;
import java.util.UUID;

import static org.junit.Assert.*;

public class PlayerDataTest {
    @Test
    public void encode() {
        int id = 382938;
        PlayerData pd = new PlayerData(id,getRandomByte(),getRandomByte(),getRandomByte(),getRandomByte(),getRandomByte());
        PlayerData pd2 = PlayerData.decode(PlayerData.encode(pd));
        Assert.assertEquals(pd.toString(),pd2.toString());
    }

    @Test
    public void testGenerateXyHpAp() {
        for (int i = 0; i < 50; i++) {
            PlayerData dragon = new PlayerData(Math.abs(UUID.randomUUID().hashCode()), true);
            dragon.generateXyHpAp();
            System.out.println(dragon);
        }
    }

    private static byte getRandomByte() {
        return (byte)new Random().nextInt(25);
    }

}
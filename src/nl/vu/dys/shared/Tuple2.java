package nl.vu.dys.shared;

public class Tuple2 extends Tuple<Integer,Integer> {
    public Tuple2(int a, int b) { super(a,b); }

    @Override
    public boolean equals(Object obj) {
        if (obj instanceof Tuple2) {
            Tuple<Integer,Integer> t = (Tuple2) obj;
            return a.equals(t.a) && b.equals(t.b);
        } else return false;
    }

    @Override
    public String toString() {
        return String.format("(%s,%s)", a,b);
    }

    @Override
    public int hashCode() {
        int hash = 1;
        hash = hash * 17 + a;
        hash = hash * 31 + b;
        return Math.abs(hash);
    }
}
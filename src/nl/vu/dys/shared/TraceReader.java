package nl.vu.dys.shared;

import nl.vu.dys.client.Bot;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TraceReader {
    private TraceReader(String file) throws IOException, InterruptedException {
        List<Bot> bots = new ArrayList<>();

        Path trace = Paths.get(System.getProperty("user.dir") + "/traces/" + file);
        InputStream is = Files.newInputStream(trace, StandardOpenOption.READ);
        Pattern patternLogin = Pattern.compile("([0-9]{10}).*(?<=PLAYER_LOGIN)");
        Pattern patternLogout = Pattern.compile("([0-9]{10}).*(?<=PLAYER_LOGOUT)");
        byte[] buf = new byte[4096];
        int a=0,b=0;
        while (is.read(buf)!=-1) {
            Thread.sleep(500);
            Matcher matcherLogin = patternLogin.matcher(new String(buf));
            Matcher matcherLogout = patternLogout.matcher(new String(buf));
            boolean loginFound, logoutFound = false;
            while ((loginFound=matcherLogin.find()) || (logoutFound=matcherLogout.find())) {
                Thread.sleep(3);
//                System.out.println("loginFound: " + loginFound + ", logoutFound=" + logoutFound);
                if (loginFound) {
                    Bot bot = new Bot();
                    bots.add(bot);
                    bot.start();
                    a++;
                }
                if (logoutFound) {
                    if (b>a) {
                        int indexToRemove = new Random().nextInt(bots.size());
                        bots.get(indexToRemove).interrupt();
                        bots.remove(indexToRemove);
                    }
                    b++;
                }
            }
        }
        System.out.println("Amount of logins: " + a);
        System.out.println("Amount of logouts: " + b);
        System.out.println("Ended");
    }
    public static void main(String[] args) throws IOException, InterruptedException {
        new TraceReader("WowSession_Node_Player_Fixed_Dynamic.csv");
    }
}

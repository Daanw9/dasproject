package nl.vu.dys.shared;

import java.nio.ByteBuffer;

/*TCP/ IP Packet that Retains the attributes for a given Player or Dragon*/
public class PlayerData {
    public final static int MTU=10;
    public int playerId;
    public byte x,y,hp,ap,isDragon=0,initialHp;
    private PlayerData() {}
    public PlayerData(int id) { this.playerId = id;}
    public PlayerData(int id, boolean isDragon) { this(id); this.isDragon=isDragon ? (byte)1 : 0; }
    public PlayerData(int id, byte x, byte y, byte hp, byte ap, byte initialHp) {
        this(id); this.x=x; this.y=y; this.hp=hp; this.ap=ap; this.initialHp=initialHp;
    }
    public static byte[] encode(PlayerData pd) {
        ByteBuffer bb = ByteBuffer.allocate(MTU);
        bb.putInt(pd.playerId);
        bb.put(pd.x); bb.put(pd.y);
        bb.put(pd.hp); bb.put(pd.ap);
        bb.put(pd.initialHp); bb.put(pd.isDragon);
        return bb.array();
    }
    public static PlayerData decode(byte[] bytes) {
        PlayerData pd = new PlayerData();
        ByteBuffer bb = ByteBuffer.wrap(bytes);
        pd.playerId=bb.getInt();
        pd.x=bb.get(); pd.y=bb.get();
        pd.hp=bb.get(); pd.ap=bb.get();
        pd.initialHp=bb.get();
        pd.isDragon=bb.get();
        return pd;
    }
    public void generateXyHpAp() {
        String aa = String.valueOf(playerId);
        String a = aa.substring(aa.length()/2,aa.length()/2+1);
        if (isDragon==0) {
            hp = asByte(Integer.valueOf(a)+10);
            ap = asByte(Integer.valueOf(a)+1);
        } else {
            hp = asByte(Integer.valueOf(a)*5+50);
            ap = asByte(Integer.valueOf(a)*2+5);
            if (ap > 20) ap=20;
        }
        x = asByte(Integer.valueOf(aa.substring(aa.length()/4,aa.length()/4+2)) % 25);
        y = asByte(Integer.valueOf(aa.substring((int)Math.floor(aa.length()/1.33),(int)Math.floor(aa.length()/1.33)+2)) % 25);
        this.initialHp=hp;
    }

    public int hashCode() {
        int i=playerId+x+y+hp+ap;
        return String.valueOf(i).hashCode();
    }

    private static byte asByte(Integer i) {
        return i.byteValue();
    }

    public String toString() {
        return String.format("x:%d,y:%d,hp:%d,ap:%d,initialHp:%d,isDragon:%d",x,y,hp,ap,initialHp,isDragon);
    }
}

package nl.vu.dys.shared;

import java.io.IOException;
import java.net.*;
import java.util.Map;
import java.util.concurrent.*;


// DatagramSocket is a packet based socket.
public class GameSocket extends DatagramSocket {

    private String host = "localhost";

    //  Bind GameSocket to the Local Socket Specified
    public GameSocket(int port) throws SocketException {
        super(port);
    }

    // Receive a Datagram Packet in a Synchronized Fashion ( Blocking of other calls to this  function until complete)
    //      via the UDP Channel. (Given the DatagramSocket Extension).
    public synchronized Protocol receive() throws IOException {

        byte[] received = new byte[Protocol.MTU];
        receive(new DatagramPacket(received,received.length));      // Send Datagram via UDP
        return Protocol.decode(received);                           // Return a Decoded Packet
    }

    // Send a Datagram Packet via UDP Channel.
    //      NOTE:  Sending of Packets is not Synchronized,  as this would imply that turns per player would be in
    //      sequence rather than parallel
    public void send(Protocol p) throws IOException {
        byte[] msg = Protocol.encode(p);
        DatagramPacket packet = new DatagramPacket(msg, msg.length, InetAddress.getByName(host), p.destinationPort);
        send(packet);
        try { Thread.sleep(3); }
        catch (InterruptedException e) { e.printStackTrace(); }
    }
}

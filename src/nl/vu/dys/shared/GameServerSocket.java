package nl.vu.dys.shared;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;


/*
* Socket implementing the protocol logic for a game server to which players connect directly
* Given that players use UDP, this game server will expose sockets to the UDP protocol
* */
public class GameServerSocket extends GameSocket {
    // Map containing all Player information:  Structure is as follows -- <Player ID <Player IP-Address,  Port>>
    Map<Integer, Tuple<InetAddress, Integer>> playerAddresses = new ConcurrentHashMap<>(100);

    // Constructor that binds the game server socket to a given Port.
    public GameServerSocket(int port) throws SocketException {
        super(port);
        setReceiveBufferSize(5000);
    }

    // Receive a datagram packet and verify if this player's packet is also sent by a player that has not been encountered before
    //       if so, update the playerlist
    public synchronized Protocol receive() throws IOException {
        byte[] received = new byte[Protocol.MTU];                           // Buffer Retains the Datagram in bytes with a predefined MTU (Size)
        DatagramPacket p = new DatagramPacket(received, received.length);   // Define a new Datagram packet for the UDP layer with a given buffer
        receive(p);                                                         // Receive any packets and place it in the "buffer datagram"
        Protocol protocol = Protocol.decode(received);                      // Un-Marshal the packet receive and create an object

        if (!playerAddresses.containsKey(protocol.clientId))                // If the 'Player List' (playerAddresses) does not contain the  current player
            playerAddresses.put(protocol.clientId, Tuple.tuple(p.getAddress(), p.getPort()));   //      add this player to the player list, and add its address an dport

        return protocol;                                                    // Return the Packet Received
    }

    // Send a packet to a specified Connection (As defined by p)
    public void send(Protocol p) throws IOException {
        byte[] msg = Protocol.encode(p);                // Marshall the packet to be sent by UDP
        DatagramPacket packet = new DatagramPacket(msg, msg.length, playerAddresses.get(p.clientId).a, playerAddresses.get(p.clientId).b);  // Define a Datagram Packet
        send(packet);
    }

    // Send a packet to entire playerAddress list rather than a single player
    public void send(Protocol p, boolean broadcast) throws IOException {
        if (broadcast) {
            byte[] msg = Protocol.encode(p);            // Marshall the datagram to be sent by UDP


            // Iterate throguh the list of players and their addresses and send the Datagram packet
            for (Tuple<InetAddress, Integer> val : playerAddresses.values()) {
                DatagramPacket packet = new DatagramPacket(msg, msg.length, val.a, val.b);
                send(packet);
            }
        }
    }

    //  Disconnecting implies in this case the removal of a player fro mthe directory.
    public void disconnect(Protocol p) {
        if (p.operation == Operation.DISCONNECT) {
            playerAddresses.remove(p.clientId);
        }
    }
}

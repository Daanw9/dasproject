package nl.vu.dys.cluster;

import nl.vu.dys.shared.PlayerData;
import nl.vu.dys.shared.Protocol;
import nl.vu.dys.shared.Tuple;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.ByteBuffer;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;
import java.util.stream.Collectors;

import static nl.vu.dys.shared.Operation.*;

import static nl.vu.dys.shared.Tuple.tuple;

/*
* Represents a server as part of a cluster
* */
class ServerJob {
    Tuple<InetAddress, Integer> launch() throws Exception {
        ServerSocket serverSocket;                                                          // Create a new Socket and  and bind it to the Defined Port
        serverSocket = new ServerSocket(new Random().nextInt(3000) + 2000);    //       Let server port be: 2000 < Port < 5000.
        final ServerSocket ss = serverSocket;                                               //       Server Address is defined to the the local IP address
                                                                                            //       Max pending connnections to the socket default to 50


        new Thread(()->{
            Map<Integer,PlayerData> playerData = new ConcurrentHashMap<>();                 // Let the Server Store Player Data for all Players

            try {
                System.out.println(String.format("ServerJob: accepting at %s:%d", InetAddress.getLocalHost().toString(), ss.getLocalPort()));
                final Socket clientSocket = ss.accept();                                    // Listen for pending connections to the Socket.
                final OutputStream os = clientSocket.getOutputStream();                     // Fetch output streams to write messages out from the socket.
                System.out.println(String.format("ServerJob: accepted at %s:%d", InetAddress.getLocalHost().toString(), ss.getLocalPort()));

                /* check if there are players which dragons can attack */
                new Thread(()->{
                    while (true) {
                        // Poll for available players to attack every 5000 millis
                        try { Thread.sleep(5000); }
                        catch (InterruptedException e) { e.printStackTrace(); break; }

                        // Skim through all of the player's Data and find all non-dragon players
                        List<PlayerData> tmpPlayerCache = playerData.values().
                                stream().filter(p->p.isDragon==0).collect(Collectors.toList());

                        // Find all dragons, and for each dragon,  find out all players that reside within a distance
                        //      less than 2 squares.  For players that meet this criteria
                        playerData.values().stream()
                                .filter(p->p.isDragon==1)
                                .forEach(dragon->
                                    tmpPlayerCache.stream()
                                        .filter(pr->getDistance(dragon,pr) <= 2)
                                        .forEach(player->{
                                            Protocol protocol = Protocol.getNewInstance();
                                            protocol.clientId=dragon.playerId;                  // Dragon from the Dragon List attacks
                                            protocol.targetClientId=player.playerId;            //      this player in the Player list
                                            protocol.sourcePort = 0; protocol.destinationPort = 0;
                                            protocol.sequenceNr = dragon.playerId / 2;
                                            protocol.operation = STRIKE;
                                            player.hp -= dragon.ap;                             // Reduce the Player Health
                                            if (player.hp <=0) {                                // Verify if the player is dead
                                                playerData.remove(player.playerId);             // If the player is dead,  remove the player from the server
                                                protocol.operation = KILLED;
                                            }
                                            protocol.payload = PlayerData.encode(player);       // Make a Packet for the operations as defined above
                                            try { os.write(Protocol.encode(protocol)); }        // Send Packet through TCP to the Communicator
                                            catch (IOException ioe) { ioe.printStackTrace(); }
                                        })
                                );
                    }
                }).start();

                // Spawn a Thread upon Launch to handle Player requests
                new Thread(()->{
                    while(true) {
                        try {
                            //  Fetch input stream for the client.
                            InputStream is = clientSocket.getInputStream();
                            //  Define buffer of size MTU
                            byte[] received = new byte[Protocol.MTU];
                            //  Read any Requests made by players until the end
                            while (is.read(received) != -1) {
                                Protocol p = Protocol.decode(received);
                                PlayerData pd = null;
                                if (playerData.containsKey(p.clientId)) {
                                    pd = playerData.get(p.clientId);
                                } else if (p.operation.equals(CONNECT)) {
                                    // Requests to Connect.  Will initialize all player data, and encapsulate all information in a packet
                                    /* Player requests to connect */
                                    boolean isDragon = p.payload.length == PlayerData.MTU && p.payload[PlayerData.MTU-1] == 1;
                                    pd = new PlayerData(p.clientId,isDragon);
                                    pd.generateXyHpAp();
                                    if (!isOccupied(playerData,pd.x,pd.y)) {
                                        playerData.put(p.clientId, pd);
                                    } else p.operation = COORDS_OCCUPIED;
                                    p.payload=PlayerData.encode(pd);
                                    p.sequenceNr++;
                                    os.write(Protocol.encode(p));
                                    continue;
                                } if (pd==null) continue;
                                PlayerData pdCopy = new PlayerData(0);
                                synchronized (pd) {
                                    switch (p.operation) {
                                        // Request to Synchronize a players local data to with the server's data (Usually Done at the Beginning)
                                        // Will create a special packet which will contain the information of all players in the server.
                                        case SYNC:
                                            synchronized (playerData) {
                                                ByteBuffer bf = ByteBuffer.allocate(1+PlayerData.MTU*playerData.size());
                                                bf.put((byte)playerData.size());
                                                for (PlayerData pdata : playerData.values())
                                                    bf.put(PlayerData.encode(pdata));
                                                p.payload=bf.array();
                                            }
                                            break;
                                        //  Player Request to Move.  Make sure the player is within bounds and make sure that
                                        //      the players requested space is not occupied
                                        case UP:    if (pd.y+1<25) if (!isOccupied(playerData,pd.x,(byte)(pd.y+1))) pd.y++; break;
                                        case DOWN:  if (pd.y+1>=0) if (!isOccupied(playerData,pd.x,(byte)(pd.y-1))) pd.y--; break;
                                        case RIGHT: if (pd.x+1<25) if (!isOccupied(playerData,(byte)(pd.x+1),pd.y)) pd.x++; break;
                                        case LEFT:  if (pd.x-1>=0) if (!isOccupied(playerData,(byte)(pd.x-1),pd.y)) pd.x--; break;
                                        // Player request to Strike another player.  Make sure the player is within bounds to
                                        //      Strike and that the target player to be striked exists.  Also make sure the player
                                        //      that requests the Strike is still alive.
                                        case STRIKE:
                                            if (!playerData.containsKey(p.targetClientId)) {
                                                System.err.println("The id does not exist");
                                                p.operation=KILLED; break;
                                            } else if (!playerData.containsKey(p.clientId)) { break; }
                                            PlayerData targetPlayer;
                                            synchronized (targetPlayer = playerData.get(p.targetClientId)) {
                                                try {
                                                    if (targetPlayer.isDragon==0) break; /* Players can only strike dragons */
                                                    if (getDistance(pd, targetPlayer) <= 2) {
                                                        targetPlayer.hp -= pd.ap;
                                                        if (targetPlayer.hp <=0) {
                                                            playerData.remove(targetPlayer.playerId);
                                                            p.operation=KILLED;
                                                        }
                                                        pdCopy = getPdCopy(p, targetPlayer);
                                                    }
                                                } catch (NullPointerException npe) {
                                                    p.operation=KILLED;
                                                }
                                                break;
                                            }
                                        case HEAL:
                                            if (!playerData.containsKey(p.targetClientId)) {
                                                System.err.println("The id does not exist");
                                                p.operation=KILLED; break;
                                            } else if (!playerData.containsKey(p.clientId)) break;
                                            synchronized (targetPlayer = playerData.get(p.targetClientId)) {
                                                if (targetPlayer.isDragon==1) break;        /* Players can only heal players */
                                                if (getDistance(pd, targetPlayer) <= 5) {   // is the Target Player Within Range?
                                                    if (targetPlayer.hp+pd.ap > targetPlayer.initialHp)
                                                        targetPlayer.hp=targetPlayer.initialHp;
                                                    else targetPlayer.hp += pd.ap;
                                                    pdCopy = getPdCopy(p, targetPlayer);
                                                }
                                                break;
                                            }
                                        case DISCONNECT:
                                            playerData.remove(p.clientId);
                                            p.operation=DISCONNECT; break;
                                        default: break;
                                    }
                                }
                                if (EnumSet.of(UP,RIGHT,DOWN,LEFT, CONNECT, DISCONNECT).contains(p.operation) && check(pd)) {
                                    p.payload=PlayerData.encode(pd);

                                } else if (EnumSet.of(STRIKE,HEAL,KILLED).contains(p.operation)) {
                                    p.payload=PlayerData.encode(pdCopy);
                                }
                                p.sequenceNr++;
                                os.write(Protocol.encode(p));                                   // Write a packet containing the Response to the Players request
                            }
                        } catch (IOException e) { e.printStackTrace(); break; }
                    }
                }).start();
            } catch (IOException ioe) { ioe.printStackTrace(); }
        }).start();

        return tuple(serverSocket.getInetAddress(), serverSocket.getLocalPort());
    }

    private static boolean isOccupied(Map<Integer,PlayerData> playerData, byte x, byte y) {
        return playerData.values().stream().anyMatch(f->f.x==x&&f.y==y);
    }

    // Check if Players are within bounds
    // WARNING:  COORDINATES ARE NOT ON GLOBAL AND ARE HARDCODED INSTEAD
    private static boolean check(PlayerData pd) {
        return pd != null && pd.x<25 && pd.x>=0 && pd.y<25 && pd.y>=0;
    }

    private static PlayerData getPdCopy(Protocol p, PlayerData targetPlayer) {
        return new PlayerData(p.targetClientId, targetPlayer.x,
                targetPlayer.y,targetPlayer.hp,
                targetPlayer.ap,targetPlayer.initialHp);
    }

    private static int getDistance(PlayerData p1, PlayerData p2) {
        int nx = Math.max(p1.x,p2.x)-Math.min(p1.x,p2.x);
        int ny = Math.max(p1.y,p2.y)-Math.min(p1.y,p2.y);
        return (nx+ny);
    }
}

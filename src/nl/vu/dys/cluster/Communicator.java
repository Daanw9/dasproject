package nl.vu.dys.cluster;

import nl.vu.dys.shared.Protocol;
import nl.vu.dys.shared.Tuple;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.*;
import java.util.*;
import java.util.concurrent.*;
import java.util.stream.Collectors;
import java.util.stream.IntStream;

class Communicator {
    private static final int INSTANCES = 4;                                                     // Define the Number of Servers to be used
    private static List<Socket> clusterGroup = new ArrayList<>();                               // List of servers, to be a list of TCP Sockets
    private ScheduledExecutorService processMessageService = Executors.newScheduledThreadPool(1);       // To execute processes periodically, and can have at most 1 thread in the pool
    private Queue<Protocol> messages = new PriorityBlockingQueue<>();                           // Queue of Messages from Users
    private static Queue<Protocol> messagesFromCluster;                                         // Queue of messages from the Cluster
    private Callback clientListener;                                                         // Callback Function.  Passed by Coordinator.java upon Instantiation of the Communicator
    private static Object lock = new Object();

    static {
        synchronized(lock) {
            if (messagesFromCluster==null)
                messagesFromCluster = new ArrayBlockingQueue<Protocol>(500);
        }
    }
    interface Callback {
        void onRead(Protocol protocol);
    }

    // Function that writes a bytestream to the TCP Socket
    private class WriteJob implements Runnable {
        private int iteration; private byte[] bytes;

        //  Construct a runnable "Write Job", instantiating a payload and an iteration variable
        WriteJob(int iteration, byte[] bytes) {
            this.iteration=iteration; this.bytes=bytes;
        }

        @Override
        public void run() {
            try {
                // Fetch the output stream for a given socket
                OutputStream os = clusterGroup.get(iteration).getOutputStream();
                //  Write the payload to the socket
                os.write(bytes);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
    private static Communicator[] ourInstances;

    static void launch(int pool, Callback callback) {
        try {
            // Launch the Number of Server Instances as Specified
            for (int i =0; i<INSTANCES;i++) {
                // Launch server instance
                Tuple<InetAddress, Integer> t = new ServerJob().launch();
                // Add Server Instance to the Cluster
                clusterGroup.add(new Socket(t.a, t.b));
            }

            // Initialize the Directory of Communicators (array of communicators) containing 'pool' number of communicators
            ourInstances = new Communicator[pool];

            // Initialize all communicators in the Directory.  Communicators make use of the callback
            for (int i=0;i<pool;i++) ourInstances[i]=new Communicator(callback);
        } catch (Exception e) { e.printStackTrace(); }
    }
    static Communicator[] getinstances() { return ourInstances; }

    // Return a random communicator from the communicator directory
    static Communicator getAnInstance() { return ourInstances[new Random().nextInt(ourInstances.length)]; }

    // Dynamic Block . This block is run when the 'Communicator' Class is constructed
    {
        processMessageService.scheduleAtFixedRate(()->{
            Queue<Protocol> tmpMessages = new PriorityQueue<>();
            int size = messages.size();
            if (size > 0) {
                /* assign all received messages to a buffer so the main queue is available for new incoming messages again */
                for (int i = 0; i < size; i++)
                    tmpMessages.offer(messages.poll());
                messages.clear();
                for (int i=0;i<size;i++) {
                    processMessage(tmpMessages.poll());
                }
            }
        }, 0, 50, TimeUnit.MILLISECONDS);
        new Thread(()->{ /* keep checking for new messages from the cluster */
            while (true) {
                // Keep polling for Message availability until you find one
                try { Thread.sleep(50); } catch (InterruptedException e) { e.printStackTrace(); break; }
                if (messagesFromCluster.size()==0) continue;

                // Check the next message from the Cluster's Message Queue and verify the sequence number.  Set the
                //      Sequence number to that value
                int sequenceNr = messagesFromCluster.peek().sequenceNr;

                // Fetch the Messages that contain the expected sequence number and remove duplicates
                Queue<Protocol> received = new ArrayDeque<>(messagesFromCluster.stream().
                        filter(p -> p.sequenceNr == sequenceNr).collect(Collectors.toList()));
                messagesFromCluster.removeIf(p -> p.sequenceNr==sequenceNr);

                //
                new Thread(()->{
                    /* Create a frequency distribution */
                    Map<Protocol, Integer> frequencies = new HashMap<>(INSTANCES);
                    IntStream.range(0,received.size()).forEach(i->{
                        Protocol poll = received.poll();
                        if (frequencies.containsKey(poll)) frequencies.put(poll,frequencies.get(poll)+1);
                        else frequencies.put(poll,0);
                    });
                    SortedSet<Map.Entry<Protocol, Integer>> sorted = new TreeSet<>(Map.Entry.comparingByValue());
                    sorted.addAll(frequencies.entrySet());
                    Protocol toSend = sorted.first().getKey();

                    // Use the Callback to check what the current request for the 'toSend' message is, and put in on
                    //      an instance's queue to execute
                    clientListener.onRead(toSend);
                }).start();
            }
        }).start();
    }

    void addMessageToQueue(Protocol p) { messages.offer(p); }

    private Communicator(Callback callback) throws SocketException, UnknownHostException {
        this.clientListener = callback;
        /* Setup a hierarchical cluster group */
        clusterGroup.forEach(serverSocket ->{
            try {
                new Thread(()->{

                    // Keep fetching Messages sent by the server
                    while(true) {
                        try {
                            InputStream is = serverSocket.getInputStream();
                            byte buf[] = new byte[Protocol.MTU];
                            while (is.read(buf) != -1) {
                                // If a message from the server has been received, add it to the queue to be processed later
                                Protocol received = Protocol.decode(buf);
                                if (received.clientId!=0) {
                                    messagesFromCluster.add(received);
                                }
                            }
                        } catch (IOException e) { e.printStackTrace(); break; }
                    }}).start();
            } catch (Exception e) { e.printStackTrace(); }
        });
    }

    private void processMessage(Protocol p) {
//        System.out.println("Communicator: Going to check seqnr " + p.sequenceNr + ": " + p.operation);
        for (int i=0;i<clusterGroup.size();i++)
            // Write the Packet to the TCP Socket
            new Thread(new WriteJob(i, Protocol.encode(p))).start();
    }

    void closeCluster() {
        System.out.println("Communicator: cluster is shutting down.");
        processMessageService.shutdown();
        clusterGroup.forEach(s -> {
            try {
                s.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}

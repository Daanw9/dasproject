package nl.vu.dys.cluster;


import nl.vu.dys.shared.GameServerSocket;
import nl.vu.dys.shared.PlayerData;
import nl.vu.dys.shared.Protocol;

import java.io.*;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.vu.dys.shared.Operation.CONNECT;

/**
 * The Coordinator is responsible for receiving messages from the client
 * and propagating a response to it, as received from the Communicator.
 */
public class Coordinator {
    private static final int PORT = 6777;               // Define the Game server socket
    private final static int AMOUNT_OF_DRAGONS =20;     // Determine the number of dragons for the Game
    private static Coordinator coordinator;
    private AtomicInteger ii = new AtomicInteger(0);
    // Bind a GameServerSocket to the defined port.
    private GameServerSocket serverSocket = new GameServerSocket(PORT);
    // This block is executed during Construction of an object
    {
        // Launch 'pool' number of communicators, and pass a callback function (Defined through Lambda).
        //      Callback function itself receives a datagram packet from the Serverside, and processes it
        Communicator.launch(1, (protocol)->{
            System.out.println("received " + protocol);
            switch (protocol.operation) {
                case COORDS_OCCUPIED:
                    protocol.operation=CONNECT;
                    protocol.clientId = Math.abs(UUID.randomUUID().hashCode());
                    Communicator.getAnInstance().addMessageToQueue(protocol);
                    break;
                case SYNC:
                    try { serverSocket.send(protocol); }
                    catch (IOException ioe) { ioe.printStackTrace(); }
                    break;
                case SHUTDOWN:      Communicator.getAnInstance().closeCluster();    break;
                case DISCONNECT:
                    serverSocket.disconnect(protocol);
                    broadcast(protocol);
                    break;
                default:
                    broadcast(protocol);
                    break;
            }
        });
        new Thread(()->{ // keep polling for messages from the client
            while(true) {
                try {
                    Protocol p = serverSocket.receive();
                    Communicator.getAnInstance().addMessageToQueue(p);
                } catch (IOException e) { e.printStackTrace(); break; }
        }}).start();
    }
    private Coordinator() throws SocketException {
        System.out.println("Coordinator launching");
    }

    // "Singleton Constructor"  of the game Instance.  A coordinator will manage all players
    //      connected to the game
    static Coordinator getInstance() {
        if (coordinator == null) {
            try {
                coordinator = new Coordinator();
            } catch (SocketException e) {
                e.printStackTrace();
            }
        }
        return coordinator;
    }

    // Upon Launch,  Spawn a 'dragons' (or n) number of dragons in the game
    private static void launch(int dragons) {
        Coordinator.getInstance();
        for (int i = 0; i < dragons; i++) {
            int dragonId = Math.abs(UUID.randomUUID().hashCode());
            PlayerData pd = new PlayerData(dragonId,true);
            Protocol protocol = Protocol.getNewInstance();
            protocol.clientId=Math.abs(UUID.randomUUID().hashCode());
            protocol.sourcePort = 0; protocol.destinationPort = 0;
            protocol.sequenceNr = new Random().nextInt(Integer.MAX_VALUE/2);
            protocol.operation = CONNECT; protocol.payload = PlayerData.encode(pd);
            Communicator.getAnInstance().addMessageToQueue(protocol);
        }
    }

    // Broadcast a message to all clients
    private void broadcast(Protocol p) {
        try {
            System.out.println("Sending: " + ii.getAndIncrement());
            serverSocket.send(p,true);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Upon Gamestart, spawn a number of dragons
    public static void main(String[] args) throws IOException {
        launch(AMOUNT_OF_DRAGONS);
    }
}

package nl.vu.dys.client;

import nl.vu.dys.client.domain.Action;
import nl.vu.dys.client.domain.BattleParticipant;
import nl.vu.dys.client.domain.Dragon;
import nl.vu.dys.client.domain.Player;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicReference;

import static nl.vu.dys.client.domain.Action.*;

/*
* Implements game logic for all bots in the game.
* */
public class Bot extends Thread {
    private ClientProxy client = null;

    @Override
    public void run() {
        super.run();
        try {
            client = new ClientProxy();
            Semaphore sem = new Semaphore(0);
            /* The current player */
            AtomicReference<BattleParticipant> bot = new AtomicReference<>();

            //
            client.setListener((action, participant) -> {
                switch(action) {
                    case CONNECT:
                        if (participant.getPlayerId()==client.getClientId()) {
                            bot.set(getCopy(participant));
                            sem.release();
                        }
                        break;
                }
            });
            client.connect();
            sem.acquire();
            /* sort the queue on most nearby */
            Queue<BattleParticipant> dragons = new PriorityBlockingQueue<>(20,
                    Comparator.comparingInt(value -> value.getDistance(bot.get())));
            Queue<BattleParticipant> players = new PriorityBlockingQueue<>(100);
            Action[] actions = new Action[] {UP,DOWN,RIGHT,LEFT};
            final Thread thread = new Thread(()->{
                while (true) {
                    try {
                        Thread.sleep(4000);
                        /* implement player action */
                        BattleParticipant me= bot.get();
                        /* check if there is a nearby players that needs healing */

//                        client.performAction(actions[new Random().nextInt(4)]);

                        BattleParticipant toHeal = null;
                        for (BattleParticipant check : players) {
                            if (check.getDistance(me) <= 3 && check.getHp() <= check.getInitialHp() / 2) {
                                toHeal = check; break;
                            }
                        }
                        if (toHeal != null) {
                            System.out.println("Healing id: " + toHeal.getPlayerId());
                            client.performAction(HEAL,toHeal.getPlayerId());
                        } else
                        if (dragons.size()>0) {
                            /* Strike a dragon if it's at most 2 squares away */
                            if (me.getDistance(dragons.peek()) <= 2) {
                                client.performAction(STRIKE, dragons.peek().getPlayerId());
                            } else  client.performAction(direction(me, dragons.peek()));
                        }
                    } catch (InterruptedException e) {
                        try {
                            client.close();
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                        System.err.println("Player killed.");
                        break;
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            });

            client.setListener((action, participant) -> {
                switch(action) {
                    case CONNECT:
                        players.add(getCopy(participant));
                        break;
                    case UP: case DOWN: case RIGHT: case LEFT: case STRIKE: case HEAL:
                        if (participant.getPlayerId()==client.getClientId()) {
                            bot.set(getCopy(participant));
                        } else {
                            players.remove(participant);
                            players.add(getCopy(participant));
                        }
                        break;
                    case KILLED: case DISCONNECT:
                        if (participant.getPlayerId()==client.getClientId()) {
                            thread.interrupt();
                        } else {
                            dragons.remove(participant);
                            players.remove(participant);
                        }
                        break;
                    default: break;
                }
            });
            client.setSyncListener(participants -> {
                participants.stream().filter(pr->pr.getType()== BattleParticipant.Type.DRAGON)
                        .forEach(d->dragons.add(getCopy(d)));
                participants.stream().filter(pr->pr.getType()== BattleParticipant.Type.PLAYER)
                        .forEach(d->{if(d.getPlayerId()!=bot.get().getPlayerId())players.add(getCopy(d));});
                thread.start();
            });
            client.performAction(Action.SYNC);
        } catch (IOException | InterruptedException e) {
            System.out.println("Player thread interrupted");
            if (client != null) try { client.close(); }
            catch (IOException e1) { e1.printStackTrace(); }
            e.printStackTrace();
        }
    }

    private static Action direction(BattleParticipant me, BattleParticipant other) {
        if (new Random().nextInt(2)==0) {
            if (other.getXPos() > me.getXPos()) return RIGHT;
            else if (other.getXPos() < me.getXPos()) return LEFT;
            else if (other.getYPos() < me.getYPos()) return DOWN;
            else return UP;
        } else {
            if (other.getYPos() < me.getYPos()) return DOWN;
            else if (other.getYPos() > me.getYPos()) return UP;
            else if (other.getXPos() > me.getXPos()) return RIGHT;
            else return LEFT;
        }
    }

    public static void main(String[] args) throws InterruptedException {
            // Start a set of 100 Bots
            for (int i = 0; i < 100; i++) { Thread.sleep(100); new Bot().start(); }
    }
    private static BattleParticipant getCopy(BattleParticipant p2) {
        switch (p2.getType()) {
            case DRAGON:
                return new Dragon(p2.getPlayerId(),p2.getXPos(),p2.getYPos(),p2.getHp(),p2.getAp(),p2.getInitialHp());
            case PLAYER:
                return new Player(p2.getPlayerId(),p2.getXPos(),p2.getYPos(),p2.getHp(),p2.getAp(),p2.getInitialHp());
            default:return null;
        }
    }
}

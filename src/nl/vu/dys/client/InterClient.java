package nl.vu.dys.client;

import javafx.application.Application;
import javafx.application.Platform;
import javafx.event.EventHandler;
import javafx.geometry.HPos;
import javafx.geometry.VPos;
import javafx.scene.Node;
import javafx.scene.Scene;
import javafx.scene.control.ContextMenu;
import javafx.scene.control.Control;
import javafx.scene.control.MenuItem;
import javafx.scene.control.Tooltip;
import javafx.scene.input.ContextMenuEvent;
import javafx.scene.paint.Paint;
import javafx.scene.shape.Circle;
import javafx.stage.Stage;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.Semaphore;

import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.RowConstraints;
import javafx.scene.layout.StackPane;
import nl.vu.dys.client.domain.Action;
import nl.vu.dys.client.domain.BattleParticipant;
import nl.vu.dys.shared.Tuple;
import nl.vu.dys.shared.Tuple2;

import static nl.vu.dys.client.ClientProxy.BATTLEFIELD_SIZE;

public class InterClient extends Application {
    private ContextMenu contextMenu;
    private MenuItem miAttack,miHeal;
    private static Map<Integer,Tuple<Circle,BattleParticipant>> participants = new HashMap<>();
    private ClientProxy clientProxy = new ClientProxy();

    public InterClient() {}

    private EventHandler<ContextMenuEvent> getMenuHandler(Circle circle, BattleParticipant p) {
        return (event -> {
            contextMenu.show(circle,event.getScreenX(),event.getScreenY());
            miAttack.setOnAction(e->{
                System.out.println(String.format("Attacking player %s",p.getPlayerId()));
                try { clientProxy.performAction(Action.STRIKE,p.getPlayerId()); }
                catch (IOException ioe) {ioe.printStackTrace(); }
            });
            miHeal.setOnAction(e->{
                System.out.println(String.format("Healing player %s",p.getPlayerId()));
                try { clientProxy.performAction(Action.HEAL, p.getPlayerId()); }
                catch (IOException ioe) { ioe.printStackTrace(); }
            });
        });
    }

    @Override
    public void stop() throws Exception {
        super.stop();
        clientProxy.close();
        System.exit(0);
    }

    private void initClient(GridPane root) throws IOException {
        clientProxy.connect();
        Semaphore sem = new Semaphore(0);
        clientProxy.setListener((action,participant) -> {
            switch (action) {
                case CONNECT:
                    if (participant.getPlayerId()==clientProxy.getClientId()) { sem.release(); return; }
                    Platform.runLater(()->{
                        Circle circle = new Circle();
                        circle.setFill(Paint.valueOf(action.equals(Action.CONNECT) ? "grey" : "red"));
                        circle.setRadius(BATTLEFIELD_SIZE <= 25 ? 8 : 3);
                        participants.put(participant.getPlayerId(),Tuple.tuple(circle,participant));
                        root.add(circle, participant.getXPos(), BATTLEFIELD_SIZE -1-participant.getYPos());
                        circle.setOnContextMenuRequested(getMenuHandler(circle,participant));
                        setTooltip(participant,circle);
                    });
                    break;
                case RIGHT: case LEFT: case UP: case DOWN:
                    Platform.runLater(()-> doMovement(root, participant));
                    break;
                case HEAL: case STRIKE:
                    doUpdate(participant);
                    break;
                case KILLED: case DISCONNECT: Platform.runLater(()->doRemove(root,participant)); break;
                default: break;
            }
        });
        try {
            sem.acquire();
            System.out.println("Connected");
            System.out.println("Player id : " + clientProxy.getClientId());
        }
        catch (InterruptedException e) { e.printStackTrace(); }

        clientProxy.setSyncListener(tParticipants ->
            Platform.runLater(() -> {
                /* fetch all participants through the client api */
                for (BattleParticipant participant : tParticipants) {
                    Circle circle = new Circle();
                    String color;
                    if (participant.getPlayerId()==clientProxy.getClientId()) color="blue";
                    else if (participant.getType().equals(BattleParticipant.Type.DRAGON)) color="red";
                    else color="grey";
                    circle.setFill(Paint.valueOf(color));
                    circle.setRadius(BATTLEFIELD_SIZE <= 25 ? 8 : 3);
                    InterClient.participants.put(participant.getPlayerId(),Tuple.tuple(circle,participant));
                    root.add(circle, participant.getXPos(), (BATTLEFIELD_SIZE -1)-participant.getYPos());
                    circle.setOnContextMenuRequested(getMenuHandler(circle,participant));
                    setTooltip(participant,circle);
                }
            })
        );
    }

    private void doRemove(GridPane root, BattleParticipant participant) {
        if (participants.containsKey(participant.getPlayerId())) {
            root.getChildren().remove(participants.get(participant.getPlayerId()).a);
            participants.remove(participant.getPlayerId());
        }
    }

    private void doUpdate(BattleParticipant participant) {
        if (participants.containsKey(participant.getPlayerId())) {
            Tuple<Circle, BattleParticipant> pOld = participants.get(participant.getPlayerId());
            participants.put(participant.getPlayerId(),Tuple.tuple(pOld.a,participant));
            setTooltip(participant, pOld.a);
        }
    }

    private void doMovement(GridPane root, BattleParticipant participant) {
        Tuple<Circle, BattleParticipant> pOld = participants.get(participant.getPlayerId());
        Circle c = pOld.a;
        Tuple2 oldt = new Tuple2(pOld.b.getXPos(), pOld.b.getYPos());
        Tuple2 newt = new Tuple2(participant.getXPos(), participant.getYPos());
        if (!oldt.equals(newt)) { /* checking if the player moved */
            root.getChildren().remove(pOld.a);
            root.add(c,newt.a, BATTLEFIELD_SIZE -1-newt.b);
        }
        participants.put(participant.getPlayerId(),Tuple.tuple(c,participant));
        setTooltip(participant,c);
    }

    @Override
    public void start(Stage primaryStage) {
//        List<String> args = getParameters().getRaw();
//        if (args.size()==3) {
//            Client.publicAddress=args.get(0);
//            Client.publicPort=Integer.valueOf(args.get(1));
//            ClientProxy.BATTLEFIELD_SIZE=Integer.valueOf(args.get(2));
//        }
        GridPane root = new GridPane();
        contextMenu = new ContextMenu();
        miAttack = new MenuItem("Attack player");
        miHeal = new MenuItem("Heal player");
        contextMenu.getItems().addAll(miAttack,miHeal);
        for (int row = 0; row < BATTLEFIELD_SIZE; row++) {
            for (int col = 0; col < BATTLEFIELD_SIZE; col ++) {
                StackPane square = new StackPane();
                //square.setLayoutX(35); square.setLayoutY(35);
                int dimensions = BATTLEFIELD_SIZE <= 25 ? 25 : 8;
                square.setPrefWidth(dimensions); square.setPrefHeight(dimensions);
                square.setStyle("-fx-background-color: white; -fx-border-color: darkgrey;");
                root.add(square, col, BATTLEFIELD_SIZE -1-row);
            }
        }
        for (int i = 0; i < BATTLEFIELD_SIZE; i++) {
            root.getColumnConstraints().add(new ColumnConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, HPos.CENTER, true));
            root.getRowConstraints().add(new RowConstraints(5, Control.USE_COMPUTED_SIZE, Double.POSITIVE_INFINITY, Priority.ALWAYS, VPos.CENTER, true));
        }
        try {
            initClient(root); /* Initializes the client for our game */
            clientProxy.performAction(Action.SYNC); /* synchronize the game state */
        } catch (IOException ioe) {
            ioe.printStackTrace();
            System.exit(-1);
        }

        Scene scene = new Scene(root, 1024, 1024);
        scene.setOnKeyPressed(event -> {
            try {
                switch (event.getCode()) {
                    case W: clientProxy.performAction(Action.UP);    break;
                    case S: clientProxy.performAction(Action.DOWN);  break;
                    case A: clientProxy.performAction(Action.LEFT);  break;
                    case D: clientProxy.performAction(Action.RIGHT); break;
                    case UP:
                    case DOWN:
                    case RIGHT:
                    case LEFT:
                        clientProxy.performAction(Action.valueOf(event.getCode().toString()));
                        break;
                    default: break;
                }
            } catch (IOException ioe) {  ioe.printStackTrace(); }
        });
        primaryStage.setTitle("DAS GAME");
        primaryStage.setScene(scene);
        primaryStage.show();
    }

    private void setTooltip(BattleParticipant p, Node n) {
        Tooltip tooltip = new Tooltip();
        tooltip.setText(String.format("id:%s\nhp:%s,ap:%s",p.getPlayerId(),p.getHp(),p.getAp()));
        Tooltip.install(n,tooltip);
    }

    public static void main(String[] args) {
        launch(args);
    }
}

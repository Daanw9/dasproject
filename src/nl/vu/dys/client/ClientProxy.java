package nl.vu.dys.client;

import nl.vu.dys.client.domain.Action;
import nl.vu.dys.client.domain.BattleParticipant;
import nl.vu.dys.client.domain.Dragon;
import nl.vu.dys.client.domain.Player;
import nl.vu.dys.shared.GameSocket;
import nl.vu.dys.shared.Operation;
import nl.vu.dys.shared.PlayerData;
import nl.vu.dys.shared.Protocol;

import java.io.IOException;
import java.net.SocketException;
import java.util.*;
import java.util.concurrent.*;

import static nl.vu.dys.shared.Operation.CONNECT;
import static nl.vu.dys.shared.Operation.DISCONNECT;

public class ClientProxy {
    public static Integer BATTLEFIELD_SIZE = 25;            // Define Battlefield Size
    private static final int DESTINATION_PORT = 6777;       // Define the port for the Destination
    private int sourcePort = new Random().nextInt(60000)+3000;      // Define the source port:  602999 <= Port <= 3000 as UDP Socket Supports up to this Value in Port
    private GameSocket clientSocket;                        // Define a Client Socket (UDP with a defined Protocol) for clients to communicate.
    private Protocol protocol;                              // Define a Datagram to be used in UDP which uses our protocol

    // Create a queue which waits for queue to become non-empty in order to remove an item, and
    //      waits for space to become available (as defined by initial capacity) before adding
    //      an item.
    //      NOTE:  Order of messages will be defined by OperationNr
    private Queue<Protocol> messages = new ArrayBlockingQueue<>(500);


    private IActionListener listener;
    interface SyncListener {
        void onSync(List<BattleParticipant> participants);
    }

    private SyncListener syncListener;
    {
        // Attempt to bind a socket to the sourceport if available.
        try {
            clientSocket = new GameSocket(sourcePort);
//            Thread.sleep(100);
        }

        catch (SocketException se) { se.printStackTrace(); }
        /* keep polling for new messages from the server in a new thread*/
        new Thread(()->{
            while(true) {
                try {
                    // Receive a 'Protocol' datagram from the UDP Layer
                    Protocol receive = clientSocket.receive();
                    messages.offer(receive);             // Place the Received Datagram on the Blicking Queue IF there is space
                                                             //     available within the queue (DEFINED IN CONSTRUCTOR
                                                             //     Otherwise Discard the message and return False
                } catch (IOException e) {
                    System.out.println("Client exited.");    // If the system could not fetch a Datagram (as in the case of
                                                             //      Failure to bind to a port  stop fetching messages from the server,
                    break;
                }
            }
        }).start();


        /* process messages */
            //  Spawn thread to process messages indefinitely
        new Thread(()->{
            // Poll for messages every 60 Millis for this particular thread
            while(true) {
                try { Thread.sleep(3);  }
                catch (InterruptedException e) { e.printStackTrace(); break; }

                // If messages queue is empty,  poll again on next interval
                if (messages.size()==0) continue;

                // Define a set of Datagrams to be processed
                Protocol toProcess = toProcess = messages.poll();
                // Process the Protocol Datagram
                // if requested operation from datagram is to SYNC.
                if (toProcess.operation==Operation.SYNC) {
                    // Verify that the syncListener is not NULL (As Instantiated by the Bot.java or InterClient.java)
                    if (syncListener!=null) {
                        PlayerData pd;
                        // Amount of Players is specified in UDP Datagram when operation is SYNC
                        int amtOfPlayers = toProcess.payload[0];
                        // Produce an Arraylist to represent the information of each player
                        List<BattleParticipant> participants = new ArrayList<>(amtOfPlayers);
                        // Populate a list with Updated Player Data
                        for (int b=1;b<amtOfPlayers*PlayerData.MTU;b+=PlayerData.MTU) {
                            pd = PlayerData.decode(Arrays.copyOfRange(toProcess.payload, b, b + PlayerData.MTU));
                            participants.add(participant(pd));
                        }

                        // Update local player data.
                        syncListener.onSync(participants);
                    }
                } else {
                    if (listener!=null && toProcess.payload.length > 0) {
                        synchronized(this) {
                            listener.perform(getAction(toProcess.operation),
                                    participant(PlayerData.decode(toProcess.payload)));
                        }
                    }
                }
            }

        }).start();
    }


    void setListener(IActionListener listener) { this.listener = listener; }
    void setSyncListener(SyncListener syncListener) { this.syncListener = syncListener; }

    private void connect(Protocol protocol) throws IOException {
        this.protocol=protocol;
        clientSocket.send(protocol);
        protocol.sequenceNr++;
    }

    void close() throws IOException {
        protocol.operation=DISCONNECT;
        clientSocket.send(protocol);
        clientSocket.close();
    }

    void connect() throws IOException{
        protocol = Protocol.getNewInstance();
        protocol.clientId=Math.abs(UUID.randomUUID().hashCode());
        protocol.sourcePort= sourcePort;
        protocol.destinationPort= DESTINATION_PORT;
        protocol.sequenceNr = new Random().nextInt(Integer.MAX_VALUE/2);
        protocol.operation = CONNECT;
        connect(protocol);
    }

    void performAction(Action action) throws IOException {
        if (protocol!=null) {
            switch (action) {
                case UP: case DOWN: case LEFT: case RIGHT: case SYNC: case STRIKE: case HEAL:
                    protocol.operation=getOperation(action);
                    clientSocket.send(protocol);
                    protocol.sequenceNr++;
                    break;
                case DISCONNECT:
                    close();
                default: break;
            }
        } else throw new RuntimeException("Not connected to server.");
    }

    void performAction(Action action, int targetPlayerId) throws IOException {
        protocol.targetClientId = targetPlayerId;
        performAction(action);
    }
    int getClientId() {return protocol.clientId; }
    private static BattleParticipant participant(PlayerData pd) {
        if (pd.isDragon==1) return new Dragon(pd.playerId, pd.x, pd.y, pd.hp, pd.ap, pd.initialHp);
        else return new Player(pd.playerId, pd.x, pd.y, pd.hp, pd.ap, pd.initialHp);
    }
    private static Action getAction(Operation operation) {
        return Action.valueOf(operation.toString());
    }
    private static Operation getOperation(Action action) { return Operation.valueOf(action.toString()); }

}

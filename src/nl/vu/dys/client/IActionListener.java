package nl.vu.dys.client;

import nl.vu.dys.client.domain.Action;
import nl.vu.dys.client.domain.BattleParticipant;

public interface IActionListener {
    void perform(Action action, BattleParticipant participant);
}


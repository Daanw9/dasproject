package nl.vu.dys.client.domain;

import nl.vu.dys.client.ClientProxy;
import nl.vu.dys.shared.PlayerData;

import java.util.Random;
import java.util.UUID;

public abstract class Participant implements BattleParticipant, Comparable<BattleParticipant> {
    private int tId = Math.abs(UUID.randomUUID().hashCode());
    private int initialHp, hp,ap, x, y;
    private Type type;
    Participant() {}
    Participant(int id) {tId=id;}
    Participant(int x, int y) { this.x=x; this.y=y;}
    Participant(int x, int y, int hp, int ap, int initialHp) {
        this(x,y);
        this.hp=hp; this.ap=ap;
        this.initialHp=initialHp;
    }
    Participant(int id, int x, int y, int hp, int ap, int initialHp) {
        this(x,y,hp,ap,initialHp);
        this.tId=id;
    }

    public boolean setHp(int hp) {
        this.hp=hp;
        return true;
    }

    public int getHp() {
        return hp;
    }

    public boolean setAp(int ap) {
        this.ap=ap;
        return true;
    }

    public int getAp() {
        return ap;
    }

    @Override
    public int getInitialHp() {
        return initialHp;
    }

    @Override
    public boolean equals(Object obj) {
        return obj instanceof BattleParticipant && obj.hashCode()==hashCode();
    }

    @Override
    public int hashCode() {
        return tId;
    }

    @Override
    public int getPlayerId() {
        return tId;
    }

    @Override
    public void setXPos(int x) {
        this.x=x;
    }

    @Override
    public void setYPos(int y) {
        this.y=y;
    }

    @Override
    public int getXPos() {
        return x;
    }

    @Override
    public int getYPos() {
        return y;
    }

    @Override
    public void spawn() {
        x = new Random().nextInt(ClientProxy.BATTLEFIELD_SIZE);
        y = new Random().nextInt(ClientProxy.BATTLEFIELD_SIZE);
    }

    void setInitialHp(int initialHp) { this.initialHp = initialHp; }
    protected void setType(Type type) { this.type = type; }

    @Override
    public Type getType() {
        return type;
    }

    @Override
    public String toString() {
        return String.format("Player id: %s, x:%s,y:%s, hp:%s,ap:%s,initialHp:%s,type:%s",
                tId,x,y,hp,ap,initialHp,type.toString());
    }

    @Override
    public int getDistance(BattleParticipant p2) {
        int nx = Math.max(x,p2.getXPos())-Math.min(x,p2.getXPos());
        int ny = Math.max(y,p2.getYPos())-Math.min(y,p2.getYPos());
        return (nx+ny);
    }

    @Override
    public int compareTo(BattleParticipant o) {
        return Integer.compare(tId, o.getPlayerId());
    }
}


package nl.vu.dys.client.domain;

import java.util.Random;

public class Player extends Participant {
    public Player() {
        super();
        setType(Type.PLAYER);
        setInitialHp(new Random().nextInt(10)+10);
        setHp(getInitialHp());
        setAp(new Random().nextInt(10)+1);
    }
    public Player(int id,int x, int y, int hp, int ap, int initialHp) {
        super(id,x,y,hp,ap, initialHp);
        setType(Type.PLAYER);
    }
}

package nl.vu.dys.client.domain;

import java.util.Random;

public class Dragon extends Participant {
    public Dragon() {
        super();
        setType(Type.DRAGON);
        setInitialHp(new Random().nextInt(50)+50);
        setHp(getInitialHp());
        setAp(new Random().nextInt(15)+5);
    }
    public Dragon(int id,int x, int y, int hp, int ap, int initialHp) {
        super(id,x,y,hp,ap,initialHp);
        setType(Type.DRAGON);
    }
}

package nl.vu.dys.client.domain;

import java.util.UUID;

public enum Action {
    CONNECT,DISCONNECT,LEFT,RIGHT,UP,DOWN,STRIKE,HEAL,SYNC,KILLED;
}

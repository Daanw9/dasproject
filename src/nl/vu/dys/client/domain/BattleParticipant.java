package nl.vu.dys.client.domain;

import java.util.Random;
import java.util.UUID;

public interface BattleParticipant {
    enum Type { PLAYER, DRAGON }
    int getPlayerId();
    void setXPos(int x);
    void setYPos(int y);
    int getXPos();
    int getYPos();
    int getInitialHp();
    boolean setHp(int hp); int getHp();
    boolean setAp(int ap); int getAp();
    Type getType();
    void spawn();
    int getDistance(BattleParticipant otherParticipant);
}
package nl.vu.dys.client;

import nl.vu.dys.client.domain.Action;
import nl.vu.dys.client.domain.BattleParticipant;
import nl.vu.dys.client.domain.Dragon;
import nl.vu.dys.client.domain.Player;
import nl.vu.dys.shared.PlayerData;
import org.junit.Assert;
import org.junit.Test;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.concurrent.Semaphore;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicInteger;

import static nl.vu.dys.client.domain.Action.*;
public class ClientProxyTest {
    @Test
    public void connect() throws Exception {
        ClientProxy clientProxy = new ClientProxy();
        Semaphore sem = new Semaphore(0);
        AtomicBoolean connected = new AtomicBoolean(false);
        clientProxy.setListener((action,participant)->{
            if (action.equals(CONNECT)&&participant.getPlayerId()==clientProxy.getClientId()) {
                connected.set(true);
                sem.release();
            }
            sem.release();
        });
        clientProxy.connect();
        sem.acquire();
        Assert.assertTrue(connected.get());
    }

    @Test
    public void connect100Players() throws Exception {
        Semaphore sem = new Semaphore(-99);
        /* Connect the first client */
        {
            AtomicInteger ai = new AtomicInteger(0);
            ClientProxy startProxy = new ClientProxy();
            startProxy.setListener((action,participant)->{
                int andIncrement = ai.getAndIncrement();
                System.out.println(andIncrement);
                sem.release();
            });
            startProxy.connect();
            Thread.sleep(100);
        }
        /* Connect the other clients */
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                ClientProxy clientProxy = new ClientProxy();
                try {
                    synchronized(this) {
                        clientProxy.connect();
                    }
                }
                catch (IOException e) { e.printStackTrace(); }
            }).start();
        }
        sem.acquire();
    }

    @Test
    public void connect100PlayersAndDisconnect() throws Exception {
        Semaphore sem = new Semaphore(-99);
        /* Connect the first client */
        {
            AtomicInteger ai = new AtomicInteger(0);
            ClientProxy startProxy = new ClientProxy();
            startProxy.setListener((action,participant)->{
                int andIncrement = ai.getAndIncrement();
                System.out.println(andIncrement);
                sem.release();
            });
            startProxy.connect();
            Thread.sleep(100);
        }
        /* Connect the other clients */
        ClientProxy clients[] = new ClientProxy[100];
        AtomicInteger iii=new AtomicInteger(0);
        for (int i = 0; i < 100; i++) {
            new Thread(()->{
                ClientProxy clientProxy = new ClientProxy();
                try {
                    synchronized(this) {
                        clientProxy.connect();
                        clients[iii.getAndIncrement()] = clientProxy;
                    }
                }
                catch (IOException e) { e.printStackTrace(); }
            }).start();
        }
        sem.acquire();
        for (ClientProxy client : clients) {
            client.close();
            Thread.sleep(100);
        }

    }

//    @Test
//    public void testSynchronize() throws IOException, InterruptedException {
//        Queue<BattleParticipant> participants = new ArrayBlockingQueue<>(100);
//        ClientProxy instance=null;
//        int toGo=10;
//        Semaphore sem = new Semaphore(-(toGo-1));
//        for (int i=0;i<toGo;i++) {
//            instance = new ClientProxy();
//            instance.setListener(((action, participant) -> {
//                if (action.equals(CONNECT)) {
//                    sem.release();
//                }
//            }));
//            instance.connect();
//        }
//        sem.acquire();
//        Semaphore sem2 = new Semaphore(0);
//        instance.setSyncListener(prts->{participants.addAll(prts);sem2.release();});
//        instance.performAction(Action.SYNC);
//        sem2.acquire();
//        Assert.assertTrue(participants.size()>0);
//    }
//
//    @Test
//    public void testMovement() throws IOException, InterruptedException {
//        Action[] actions = new Action[]{UP, DOWN, RIGHT, LEFT};
//        Queue<BattleParticipant> participants = new ArrayBlockingQueue<>(100);
//        int toGo = 30;
//        ClientProxy[] instances = new ClientProxy[toGo];
//        Semaphore sem = new Semaphore(-(toGo - 1));
//        for (int i = 0; i < toGo; i++) {
//            instances[i] = new ClientProxy();
//            instances[i].setListener(((action, participant) -> {
//                if (action.equals(Action.CONNECT)) {
//                    sem.release();
//                }
//            }));
//            instances[i].connect();
//        }
//        sem.acquire();
//        Thread t = new Thread(() -> {
//            con:
//            while (true) {
//                for (ClientProxy proxy : instances) {
//                    try {
//                        Thread.sleep(new Random().nextInt(500));
//                        proxy.performAction(actions[new Random().nextInt(actions.length)]);
//                    } catch (IOException | InterruptedException e) {
//                        e.printStackTrace();
//                        break con;
//                    }
//
//                }
//            }
//        });
//        t.start();
//        Thread.sleep(20000);
//        t.interrupt();
//        try { for (ClientProxy proxy : instances) proxy.close(); }
//        catch (IOException ioe) { ioe.printStackTrace();}
//    }
}